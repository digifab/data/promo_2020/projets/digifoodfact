#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 18 09:43:40 2020

Ce code a pour objectif de récuperer les données du formulaire de l'interface
web où l'utilisateur renseignera les aliments consommés et les sensations
ressenties.
De créer la table ressentis (une pour les utilisateur et une pour le programme)
et un calcul de pourcentages.
Et de renvoyer ces informations dans la table "ressentis".

Le but est de proposer une solution à un ressenti négatif à l'utilisateur,
en ce basant à la fois aux données générales mais aussi aux données propres à
l'utilisateur.
    - L'utilisateur à mangé un aliment donnant mal à la tete.
    - Si d'après les données générales la carotte est un aliment qui soulage
      les maux de tetes.
    - Alors le programme proposera la carotte à l'id_utilisateur.
    - Sauf si la carotte donne un effet négatif à cet id_utilisateur.
    - Le programme proposera alors un autre aliment qui de manière général
      aussi soulage la tete.

@author: Alexis, Reda, Pierre
"""
import pandas as pd
import random

utilisateurs = {
            'id_utilisateur':[1,2,3,4,5,6],
            'nom':['pierre','reda','alexis','cecile','joseph','lea']
            }

structure_sensation = {
                   'fatigue':0,'mal de tête':0,
                   'mal de ventre':0,'dynamisme':0,'soulagement tête'
                   :0,'soulagement ventre':0,'total':0
                    }

aliments = [
        "carotte", "petit pois", "pomme de terre", "jambon", "semoule",
        "yaourt", "escalope", "haricots verts", "tomates", "salade",
        "lardons", "pâtes", "concombre", "maïs", "pizza", "banane",
        "pomme", "abricot", "quiche"
        ]

structure_aliments = {'id_aliment':[],'aliment':[]}

i = 1
for aliment in aliments:
    structure_aliments['id_aliment'] += [i]
    structure_aliments['aliment'] += [aliment]
    i += 1


data_aliments = pd.DataFrame(structure_aliments)

data_utilisateur = pd.DataFrame(utilisateurs)

columns = ['id_utilisateur','aliment']
columns += list(structure_sensation.keys())

data_sensations = pd.DataFrame(columns=columns)

def generations_aléatoire2():

    i=0
    for id_utilisateur in data_utilisateur['id_utilisateur']:
            for aliment in aliments:

                conteners=[id_utilisateur,aliment,

                      random.randint(0,60),random.randint(0,60),
                      random.randint(0,60),random.randint(0,60),
                      random.randint(0,60),
                      random.randint(0,60),random.randint(60,80)]
                print(conteners)
                data_sensations.loc[i] = conteners
                i += 1

df_ressentis = data_sensations

dic = {
    'fatigue': 'dynamisme',
    'mal de tête': 'soulagement tête',
    'mal de ventre': 'soulagement ventre'
    }

def table_ressentis(aliments,sensation,id_utilisateur):

    df_utilisateur2 = df_ressentis[df_ressentis['id_utilisateur']
                                   == id_utilisateur]

    for aliment in aliments :
        if aliment not in str(df_utilisateur2['aliment']):
            a = len(df_ressentis)
            df_ressentis.loc[a,'id_utilisateur'] = id_utilisateur
            df_ressentis.loc[a,'aliment'] = aliment
            df_ressentis.loc[a,'total'] = 1
            for i in range(0,len(sensation)):
                df_ressentis.loc[a,sensation] = 1
            df_ressentis.fillna(0,inplace=True)
        else:
             a = df_ressentis[(df_ressentis['aliment'] == aliment) &
             (df_ressentis['id_utilisateur'] == id_utilisateur)].index
             df_ressentis.loc[a,'total'] += 1
             for i in range(0,len(sensation)):
                df_ressentis.loc[a,sensation[i]] += 1


    #transformation des données ressentit utilisateurs en pourcentage
    liste = []
    repertoire = {}

    for aliment in df_ressentis['aliment']:
        if aliment not in liste:
            liste += [aliment]


    for aliments in liste :
        data_stat = df_ressentis[df_ressentis['aliment'] == aliments]
        repertoire[aliments] = {}
        for colonne in data_stat.columns:
            if (str(colonne) != 'id_utilisateur' and
                str(colonne) != 'aliment'):
                b = (data_stat[colonne].sum()/data_stat['total'].sum())*100
                b = round(b,2)
                repertoire[aliments][colonne] = b


    df72 = pd.DataFrame.from_dict(repertoire,orient='index')
    df72 = df72.reset_index()
    liste_colonne = ['aliment']
    liste_colonne += list(df72.columns)
    del liste_colonne[1]
    df72.columns = liste_colonne

    # La partis message
    message='Merci d\'avoir renseigné tes informations '
    for nombre in range(0,len(sensation)):
        if sensation[nombre] in dic.keys():
            aide = dic[sensation[nombre]]

            sensation_utilisateur = (df_ressentis[df_ressentis
                                        ['id_utilisateur'] == id_utilisateur])

            df_solution = df72.sort_values(by=[aide],ascending=False)

            k = 0
            bouton = 0
            i = 0
            while bouton == 0 and i <= len(list(dic.keys()))-1 :

                solution = list(df_solution['aliment'])[k]

                test_aliment = (sensation_utilisateur
                              [sensation_utilisateur['aliment'] == solution])
                repertoire = {}
                repertoire[solution] = {}
                for colonne in test_aliment.columns:
                    if (str(colonne) != 'id_utilisateur' and
                        str(colonne) != 'aliment'):
                        b = (test_aliment[colonne].sum()/
                           test_aliment['total'].sum())*100
                        b = round(b,2)
                        repertoire[solution][colonne] = b

                df_stat_utilisateur = pd.DataFrame.from_dict(repertoire,
                                                           orient='index')
                df_stat_utilisateur = df_stat_utilisateur.reset_index()
                liste_colonne = ['nom_aliment']
                liste_colonne += list(df_stat_utilisateur.columns)
                del liste_colonne[1]
                df_stat_utilisateur.columns = liste_colonne

                if list(test_aliment[list(dic.keys())[i]]>=50)[0] == True:
                    k+=1

                    if i == len(list(dic.keys()))-1:
                      i=0
                      bouton += 1

                else:

                    i+=1

            message += ' Je vois que tu souffre de ' + sensation[nombre]
            message += ' la plupart de nos utilisateurs règlent '
            message += 'ce probleme avec des ' + solution

    return(message)