

liste_viandes = {}

liste_viandes["poulet"]= ["proteine_100g", "lipides_100g", "vitamines_B_100g"]
liste_viandes["boeuf"]= ["proteine_100g", "lipides_100g", "vitamines_A_100g"]
liste_viandes["agneau"]= ["proteine_100g", "lipides_100g", "vitamines_B_100g"]
liste_viandes["porc"]= ["proteine_100g", "lipides_100g", "vitamines_C_100g"]

liste_ingredients = {
    "carotte": {"glucides": 6.45, "fibres": 2.7, "lipides": 5.42},
    "pomme de terre": {"glucides": 16.7, "fibres": 1.8, "lipides": 0.86},
    "jambon": {"glucides": 1.3, "fibres": 0, "lipides": 1.3},
    "semoule": {"glucides": 72.83, "fibres": 3.9, "lipides": 0},
    "yaourt": {"glucides": 6.68, "fibres": 0, "lipides": 4.66},
    "escalope": {"glucides": 0, "fibres": 0, "lipides": 0},
    "haricots verts": {"glucides": 3.63, "fibres": 4, "lipides": 1},
    "tomates": {"glucides": 2.26, "fibres": 1.2, "lipides": 2.25},
    "salade": {"glucides": 1.37, "fibres": 1.3, "lipides": 1.7},
    "lardons": {"glucides": 0.6, "fibres": 0, "lipides": 0.46},
    "pâtes": {"glucides": 29.7, "fibres": 2.28, "lipides": 5.42},
    "petit pois": {"glucides": 4.7, "fibres": 5.8, "lipides": 0.47},
    "concombre": {"glucides": 2.04, "fibres": 0.6, "lipides": 1.34},
    "abricot": {"glucides": 7.14, "fibres": 1.8, "lipides": 6.57},
    "quiche": {"glucides": 22.2, "fibres": 0.90, "lipides": 1.7},
    "maïs": {"glucides": 15.41, "fibres": 1.7, "lipides": 4.54},
    "pizza": {"glucides": 27.7, "fibres": 2.36, "lipides": 2.6},
    "banane": {"glucides": 19.6, "fibres": 1.9, "lipides": 14.8},
    "pomme": {"glucides": 11.6, "fibres": 1.4, "lipides": 9.35}}

liste_legumes = {
    'ail': [],
    'artichaut': [],
    'asperge': [],
    'aubergine': [],
    'betterave': [],
    'blette': [],
    'brocoli': [],
    'carotte': [],
    'celeri': [],
    'champignon de paris': [],
    'chou': [],
    'chou de bruxelles': [],
    'chou-fleur': [],
    'concombre': [],
    'courge': [],
    'courgette': [],
    'cresson': [],
    'echalote': [],
    'endive': [],
    'epinard': [],
    'fenouil':[],
    'haricot vert':[],
    'laitue': [],
    'mache': [],
    'mais': [],
    'navet': [],
    'oignon': [],
    'panais': [],
    'petit pois': [],
    'poireau': [],
    'poivron': [],
    'potiron': [],
    'radis':[],
    'salsifis': [],
    'tomate': [],
    'topinambour': []}


liste_fromages={
    'roquefort':[],
    'parmesan':[],
    'comte':[],
    'chevre frais': [],
    }


liste_fruits = {
    'abricot': [],
    'ananas': [],
    'avocat': [],
    'cassis': [],
    'cerise': [],
    'chataigne': [],
    'citron': [],
    'coing': [],
    'clémentine': [],
    'figue':[],
    'fraise': [],
    'framboise': [],
    'grenade': [],
    'groseille':[],
    'kaki': [],
    'kiwi':[],
    'mandarine': [],
    'mangue': [],
    'melon': [],
    'mirabelle': [],
    'mure':[],
    'myrtille':[],
    'nectarine': [],
    'noisette':[],
    'noix':[],
    'orange': [],
    'pamplemousse':[],
    'pastèque': [],
    'pêche':[],
    'poire': [],
    'prune': [],
    'pomme':[],
    'quetsche':[],
    'raisin': [],
    'reine claude':[],
    'rhubarbe': []}

liste_allergies = {
    'gluten': [],
    'crustaces': [],
    'oeufs': [],
    'arachides': [],
    'poissons': [],
    'soja': [],
    'lait': [],
    'fruits a coque': [],
    'celeri': [],
    'moutarde': [],
    'sesame': [],
    'sulfites': [],
    'lupin': [],
    'mollusques': [],
    }



