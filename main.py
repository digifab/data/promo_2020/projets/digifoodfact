'''
CREATION D'UNE APPLICATION WEB NUTRITIONNELLE

Cette application a pour objectif de permettre à un utilisateur 
d'avoir des recommandations alimentaires en fonction de sa consommation 
personnelle, qu'il soit sportif ou non. On lui proposera des recettes en
fonction de son bilan nutritionel.

L'utilisateur aura aussi la possibilité d'avoir des renseignements sur
les fruits et légumes de saisons, sur l'alimentation BIO, et les valeurs
 nutritives des aliments.

Equipe web : Sébastien LEHOUX, Jean-Baptiste THEROULDE,
             Nicolas Detaillante, Peggy DUNOYER

DATE : JUIN 2020

'''

# LIBRAIRIES EXTERNES
from flask import Flask , render_template, redirect, url_for, flash, session
from flask_login import (LoginManager, UserMixin, 
                                login_required, login_user, logout_user,
                                    current_user)
from flask import request, Response
import numpy as np
import time
import datetime
import random as rd
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import sqlite3 as lite
from werkzeug.security import generate_password_hash, check_password_hash
import psycopg2

# Import pour autocompletion
# import json
# from wtforms import TextField, Form

# LIBRAIRIES INTERNES
import modelisation
import liste_aliments
import saisonnalite


#BASE DE DONNEES FICTIVE
ADRESSE_BASE_DE_DONNEES = "base_fictive.db"
ADRESSE_BDD_AUTH = "test116.db"


#BASE DE DONNEES "DIGIFAB"
# con = psycopg2.connect(host="90.108.145.56",
#                    database="digifab",
#                    user="postgres",
#                    password="digifab",
#                    port=5432)

con = psycopg2.connect(host="localhost",
                    database="alimentation",
                    user="postgres",
                    password="MotDePassePostgre",
                    port=5432)

cur = con.cursor()


# INITIALISATION DE L'APPLICATION AVEC FLASK
app=Flask(__name__)
app.config.update(
    DEBUG = False,
    SECRET_KEY = 'secret_xxx'
)

login_manager = LoginManager()
login_manager.login_view = "login"
login_manager.init_app(app)

@app.route('/')
def info():

    title = "Accueil"
    return render_template("index.html", title=title)


@app.route('/liste_aliments', methods=['GET', 'POST'])
def aliments():
    title = "liste_aliments"
   
    if request.method == 'POST':
        if current_user.is_authenticated : 
            date_ajout = datetime.datetime.strptime(request.form['date_ajout'],
                                            "%Y-%m-%d")
            aliment = request.form['aliment']
            quantite = request.form['quantite']
            ressenti = request.form['ressenti']
            ajout_alim = [{"date" : date_ajout, 
                            "nom" : aliment, 
                            "quantite" : quantite}]
            df_ajout_alim = pd.DataFrame(ajout_alim)
            current_user.utilisateur.sauvegarder_repas(df_ajout_alim, 
                                                       date_ajout)
        
    return render_template("liste_aliments.html", title=title,
                           liste_ingredients=liste_aliments.liste_ingredients)


@app.route('/formulaire_graphe')
def formulaire_graphe():

    '''sur cette page l'utilisateur devra remplir un formulaire de type
       date et methode post qui lui permetera de choisir la période d'analyse
       souhaitée, la date par défaut est obtenu grace à datetime.
    '''

    graphs = ['repas','bilan', 'carence']
    date_debut =  datetime.datetime(2020,1,1)

    return render_template("formulaire_graphe.html",graphs=graphs
                           ,date_debut=date_debut)


@app.route('/graph_aliments', methods=['GET', 'POST'])
def graph():
    if current_user.is_authenticated : 
    
        mode = request.form['graph']
        date_debut = datetime.datetime.strptime(request.form['date_debut'],
                                       "%Y-%m-%d")
    
    
        date_fin = datetime.datetime.strptime(request.form['date_fin'],
                                     "%Y-%m-%d")

        current_user.utilisateur.chargement_repas(date_debut=date_debut,
                                          date_fin=date_fin)

        if mode == "repas":
            image = "static/images/graph_repas.png"
            addresse = "/images/graph_repas.png"
            current_user.utilisateur.graphique(mode=mode,
                                               date_repas = date_debut,
                                               fichier = image)
    
        if mode == "bilan":
            image = "static/images/graph_bilan.png"
            addresse = "/images/graph_repas.png"
            current_user.utilisateur.graphique(mode=mode,
                                               date_debut = date_debut,
                                               date_fin = date_fin,
                                               fichier = image)
    
        if mode == "carence":
            image = "static/images/graph_carence.png"
            addresse = "/images/graph_carence.png"
            current_user.utilisateur.graphique(mode=mode,
                                               date_debut = date_debut,
                                               date_fin = date_fin,
                                               fichier = image)
            
        current_user.utilisateur.vide_df_repas()

    return render_template("graph_aliments.html", addresse=addresse,
                           time=int(time.time()))


@app.route('/recettes',methods=['GET', 'POST'])
def recettes():

    title = "recettes"
    return render_template("recettes.html", title=title)


@app.route('/saisonnalite', methods=['GET', 'POST'])
def saison():

    title = "saisonnalite"
    return render_template("saisonnalite.html", title = title,
                            liste_fruits = saisonnalite.select_fruits,
                            liste_legumes = saisonnalite.select_legumes,
                            saison = saisonnalite.saison)


@app.route('/alimentation_bio',methods=['GET', 'POST'])
def bio():

    title = "alimentation_bio"
    return render_template("alimentation_bio.html", title = title)


@app.route('/menu_sportif')
def menu_sportif():

    title = "menu_sportif"
    return render_template("menu_sportif.html", title=title)


@app.route('/recettes_sport')
def recettes_sport():

    cur.execute("""SELECT * FROM public.nutrition2""")
    rows = cur.fetchall()

    my_list = []
    for row in rows:
        my_list.append(row[0])

    title = "recettes_sport"
    return render_template("recettes_sport.html", title=title,
                           results=my_list)


@app.route('/valeurs_energetiques', methods=['GET', 'POST'])
def valeurs_energetiques():
    aliment = request.form['aliments']



    title = "valeurs_energetiques"
    return render_template("valeurs_energetiques.html", title=title)


@app.route('/ressenti', methods=['GET','POST'])
def ressenti():


    # ces deux variables serviront à recevoir les deux input
    var_ali = str(request.form['aliment'])
    quantite = float(request.form.get('quantite'))
    rst = str(request.form['ressenti'])

    # remplace la quantité en gramme par une valeur de base (les colonnes
    # étant basé sur 100g)
    var_quant= quantite/100


    sql = """select product_name as aliment, avg("energy-kcal_100g") as kcal,
    avg(proteins_100g) as proteines, avg(fiber_100g) as fibre, avg(sugars_100g)
    as sucre, avg("nutrition-score-fr_100g") as nutriscore from aliment group
    by product_name"""
    cur.execute(sql)

    df = pd.read_sql_query(sql,con)

    val_nut=df[(df['aliment']==var_ali)]


    ids=val_nut[(val_nut['aliment']==var_ali)].index[0]
    colonnes =['kcal', 'proteines', 'fibre', 'sucre', 'nutriscore']
    for colonne in colonnes :
        val_nut.loc[ids,colonne]=val_nut.loc[ids,colonne]*var_quant

    sql="""select nom_aliment, fatigue_percent,dynamisme_percent,
    lourdeur_percent,legerete_percent, maux_de_tete_percent,
    maux_de_ventre_percent from aliment_ressenti
    where nom_aliment='"""+var_ali+"';"
    cur.execute (sql)

    res_ali=pd.read_sql_query(sql,con)



    ids=res_ali[(res_ali['nom_aliment']==var_ali)].index[0]
    pos=[res_ali.loc[ids,'dynamisme_percent'],
          res_ali.loc[ids,'legerete_percent']]
    neg=[res_ali.loc[ids,'fatigue_percent'],
          res_ali.loc[ids,'lourdeur_percent'],
          res_ali.loc[ids,'maux_de_tete_percent'],
          res_ali.loc[ids,'maux_de_ventre_percent']]

    pos=sorted(pos)
    neg=sorted(neg)
    neg=neg[-2:]

    colonnespos = ['dynamisme_percent', 'legerete_percent']
    colonnespos2=['Dynamisme','Legereté']
    colonnesneg=['fatigue_percent','lourdeur_percent','maux_de_tete_percent',
               'maux_de_ventre_percent']
    colonnesneg2=['Fatigue','Lourdeur','Maux de tête','Maux de ventre']

    i=1
    colonnes=[]
    while i < 3 :
        colid=0
        for colonne in colonnespos:
            if pos[-i] == res_ali.loc[ids,colonne]:
                colonnes.append(colonnespos2[colid])
            else :
                pass
            colid+=1
        i+=1

    i=1
    while i < 3 :
        colid=0
        for colonne in colonnesneg:
            if neg[-i] == res_ali.loc[ids,colonne]:
                colonnes.append(colonnesneg2[colid])
            else :
                pass
            colid+=1
        i+=1


    EffetSecPos1=colonnes[0]
    EffetSecPos2=colonnes[1]
    EffetSecNeg1=colonnes[2]
    EffetSecNeg2=colonnes[3]

    ValEfSecPos1=int(pos[-1])
    ValEfSecPos2=int(pos[-2])
    ValEfSecNeg1=int(neg[-1])
    ValEfSecNeg2=int(neg[-2])

    #val_nut.to_html(header="true", table_id="table")
    return render_template("ressenti.html", alm = var_ali, quantite = quantite,
                           EffetSecPos1 = EffetSecPos1,
                           EffetSecPos2=EffetSecPos2, EffetSecNeg1=EffetSecNeg1,
                           EffetSecNeg2=EffetSecNeg2,ValEfSecPos1=ValEfSecPos1,
                           ValEfSecPos2=ValEfSecPos2,ValEfSecNeg1=ValEfSecNeg1,
                           ValEfSecNeg2=ValEfSecNeg2,
                           tables=[val_nut.to_html(classes='data')],
                           titles=val_nut.columns.values)


@app.route('/allergies',  methods=['GET', 'POST'])
def allergies() :

    title =allergies
    return render_template('allergies.html', title = title)


@app.route('/creation_compte')
def creation_compte():

    """Ici nous envoyons une template qui permet la creation d'un compte.
       les informations nécéssaires sont les suivante : un nom de compte
       une addresse mail, et le mot de passe désiré en double"""

    if 'nom_de_compte' in session :
        return redirect("/")
    return render_template("formulaire_creation_compte.html")


@app.route('/traitement_creation_compte', methods=['POST'])
def traitement_creation_compte() :
    """Nous traiton le contenu du formulaire obtenu de la page de création
       Il est a noté que par sécurité le mot de passe de l'utilisateur ne doit
       JAMAIS être stocké sur la BDD en clair, et de limité au maximum les
       sotckage en variable en clair, si la requête est fait en HTTPS le mdp
       sera stocké au sein du formulaire encodé et donc ne sera jamais present
       sur le site sous quelque forme que ce soit en clair"""

    #Recuperation données formulaire
    nom_de_compte = request.form['nom_de_compte']
    mail = request.form['mail']

    #Verification que les mdp sont les même
    if request.form['mot_de_passe_1'] != request.form['mot_de_passe_2'] :
        return redirect("erreur_mdp")

    #Debut des operation
    con=lite.connect(ADRESSE_BDD_AUTH)
    with con:
        #Obtenir l'id par raport au dernier utilisateur
        df = pd.read_sql_query(f"""SELECT * FROM authentification
                                   ORDER BY id_auth DESC LIMIT 1""",con)
        id_auth = df['id_auth'][0] + 1

        #Verifion si le compte existe deja pour cela on récupère les resultat
        #D'une querry pour le nom du compte dans une dataframe panda
        df = pd.read_sql_query(f"""SELECT DISTINCT nom_de_compte
                                   FROM authentification
                                   WHERE nom_de_compte = '{nom_de_compte}'"""
                                                                        , con)
        #si la querry a donné 0 resultat le compte n'existe donc pas
        if df.empty:
            #Si non sallage du mot de passe avec werkzeug security
            mot_de_passe_sale = generate_password_hash(
                                                request.form['mot_de_passe_1'])
            cur = con.cursor()
            requete_SQL = f"""
                INSERT INTO authentification VALUES('{id_auth}',
                                                    '{nom_de_compte}',
                                                    '{mail}',
                                                    '{mot_de_passe_sale}')"""
            cur.execute(requete_SQL)
            cur.close()
            #Comunique a l'utilisateur le succès
            return redirect('/creation_success')
        #Si le compte existe deja redirige vers une page qui indique l'echec
        #de sa tentative de creation de compte
        return redirect('/creation_fail')


@app.route('/creation_fail')
def creation_fail() :
    return render_template('message_creation_fail.html')


@app.route('/creation_success')
def creation_success() :
    return render_template('message_creation_success.html')



@app.route('/formulaire_authentification')
def authentification():
    if 'nom_de_compte' in session :
        return redirect("/")
    "Le formulaire de connexion se contente de récupéré l'adresse et le mdp"
    return render_template("formulaire_authentification.html")


@app.route('/traitement_authentification', methods=['GET','POST'])
def login():



    if request.method == 'POST':
        username = request.form['nom']


        #if check_password_hash(df['mot_de_passe'][0],
                                   #request.form['mot_de_passe']) :

        if request.form['mot_de_passe'] == '00000':

            #id = df['user_id'][0]
            user = modelisation.chargement_utilisateur(username)
            pseudo_class = [user.pseudo]
            id_class = [user.id_utilisateur]
            login_user(user)

            return render_template('index.html')
        else:
            return ('zut')

    else:
        return render_template("formulaire_authentification.html")

@app.route('/test_connexion')
def test_connexion() :
    print("-"*10)
    print(current_user)
    print(type(current_user))
    print(type(current_user.utilisateur))
    
    date_1 = datetime.datetime(2020,1,1)
    date_2 = datetime.datetime(2020,2,1)
    
    current_user.utilisateur.chargement_repas(date_1, date_2)
    print(current_user.utilisateur.difference_carence(date_1,date_2))
    
    print("-"*10)
    return redirect("/")

@app.route('/connexion_success')
def connexion_success() :
    return render_template('message_connexion_success.html')


@app.route('/connexion_fail')
def connexion_fail() :
    return render_template('message_connexion_fail.html')


@app.route('/renseignement',methods=['GET'])
@login_required
def renseignement() :
    """Nous récupérons les données que l’utilisateur rentre dans le formulaire :
    l’âge, la taille, le poids, le genre, s’il est sportif,
    ses allergies, ses objectifs."""
    return render_template("renseignement.html")


@app.route('/deconnexion')
def deconnexion():

    return render_template('andy_page_deconnexion.html')


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return Response('<p>Logged out</p>')

######################################################################
#------- Pour auto completion [liste_aliments.py] -------------------#
######################################################################

# class SearchFormLegume(Form):
#     autocomp = TextField('Entrez un légume', id='legume_autocomplete')
# ### pour viande
# # class SearchFormViande(Form):
# #     autocomp = TextField('Entrez un légume', id='legume_autocomplete')

# @app.route('/_autocomplete', methods=['GET'])
# def autocomplete():
#     return Response(json.dumps(new_liste_legumes), mimetype='application/json')


# @app.route('/interactive', methods=['GET', 'POST'])
# def index():
#     form = SearchFormLegume(request.form)
#     return render_template("interactive.html", form=form)


#### pour viande
# class SearchFormViande(Form):
#     autocomp = TextField('Entrez un légume', id='legume_autocomplete')


#A integrer
# new_liste_viandes
# new_liste_fromages
# new_liste_fruits
# new_liste_allergies
#######################################################################
#----------------------- Fin autocompletion  -------------------------#
#######################################################################
    
@login_manager.user_loader
def load_user(userid):
    try:
        test = modelisation.chargement_utilisateur(id_utilisateur = userid)
        
        class User(UserMixin):

            def __init__(self, id):
                self.id = id
                self.name = test.pseudo
                self.password = self.name + "_secret"
                self.utilisateur = test
                
                
            def __repr__(self):
                return "%d/%s/%s" % (self.id, self.name, self.password)
        return User(userid)

    except:
        print("Raté")
        return None

if __name__ == '__main__':
    app.run(debug=True, use_reloader = False)
