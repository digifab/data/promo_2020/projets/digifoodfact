# -*- coding: utf-8 -*-
"""
Ce module contient les méthodes d'accès aux information alimentaire contenu
en base de données.
Il sert d'appoint au module reconutritionv2.
"""

from playhouse.reflection import generate_models
from playhouse.postgres_ext import PostgresqlExtDatabase

# db = PostgresqlExtDatabase(host="90.108.145.56",
#                            database="digifab",
#                            user="postgres",
#                            password="digifab",
#                            port=5432)

db = PostgresqlExtDatabase(host="localhost",
                            database="alimentation",
                            user="postgres",
                            password="MotDePassePostgre",
                            port=5432)


class aliments():
    """Initialisation d'une class aliments vide, cette class sera
    dynamiquement rempli par peewee à l'import de ce module par les deux
    commandes suivante
    """
    pass

# Recupère des models de connexion vers la bdd sous format d'un tuple
# ("nom_de_la_table", <model_de_la_table>). 
models = generate_models(db)
# Transmet ces models au variables globales permettant ainsi leurs accès,
# remplace les instances précédente 
globals().update(models)

def requete_aliments(carence):
    """Renvoie une liste de 10 aliments qui corresponde à la carence passé 
    en paramètre.
    """
    
    query = {"glucides": (aliments
                         .select(aliments.product_name, aliments.sugars_100g)
                         .where(aliments.sugars_100g.is_null(False))
                         .order_by(aliments.sugars_100g.desc())
                         .limit(10)),
             "fibres": (aliments
                         .select(aliments.product_name, aliments.fiber_100g)
                         .where(aliments.fiber_100g.is_null(False))
                         .order_by(aliments.fiber_100g.desc())
                         .limit(10)),
             "lipides": (aliments
                         .select(aliments.product_name, aliments.fat_100g)
                         .where(aliments.fat_100g.is_null(False))
                         .order_by(aliments.fat_100g.desc())
                         .limit(10))}
    
    resultats = query[carence]
    suggestions = []
    for resultat in resultats :
        suggestions += [resultat.product_name]
    return suggestions

if __name__ == "__main__" : 

    print(requete_aliments("fibres"))